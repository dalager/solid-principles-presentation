Præsentation om SOLID principperne
===================================

SOLID illustreret ved c# kode og snak.

 * Single Responsibility Principle
 * Open/Closed Principle
 * Liskov's Substitution Principle
 * Interface Segregation Principle
 * Dependency Inversion Principle

Slides
--------------

 * Ligger i /slides/revealjs/index.html
 * hvis du vil bruge notes-funktion skal du køre i webserver:
   * npm install (i revealjs root, kræver at du har installeret npm)
   * grunt serve (starter en webserver)


Samples
--------------
 * ligger lidt blandet sammen under /src
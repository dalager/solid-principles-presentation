﻿using System;
using System.Collections.Generic;

namespace Samples
{
    /// <summary>
    /// Opretter en paycalculator.
    /// </summary>
    public class PayCalculatorFactory
    {
        private static Dictionary<Type, Type> strategies=new Dictionary<Type, Type>();

        static PayCalculatorFactory() 
        {
            strategies.Add(typeof(Employee),typeof(EmployeePayCalculator));   
            strategies.Add(typeof(Partner),typeof(PartnerPayCalculator));
        }
        public static PayCalculator Create(Person p)
        {
            var strategy = GetStrategyForPerson(p);
            return (PayCalculator) Activator.CreateInstance(strategy, p);
        }

        private static Type GetStrategyForPerson(Person p)
        {
            if (!strategies.ContainsKey(p.GetType()))
            {
                return strategies[typeof (Employee)];
            }
            return strategies[p.GetType()];
        }
    }
}
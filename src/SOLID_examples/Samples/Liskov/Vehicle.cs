﻿using System;

namespace Samples.Liskov
{
    public class Vehicle
    {
        public IEngine Engine{ get; set; }
        public INavigator Navigator { get; set; }
        public void Start(){Engine.Start();}

        public void GoTo(double lat, double lon)
        {
            Navigator.SetCourse(lat, lon);
            Engine.Forward();
        }
    }

    public interface INavigator
    {
        void SetCourse(double lat, double lon);
    }

    public class Car : Vehicle
    {
        public Car()
        {
            this.Engine = new CarEngine();
            this.Navigator = new GPSSteeringWheel();
        }
    }

    public class GPSSteeringWheel : INavigator
    {
        public void SetCourse(double lat, double lon)
        {
            Console.Out.WriteLine("Getting bearings from GPS and turning the steering wheel");
        }
    }

    public class CarEngine : IEngine
    {
        public void Start()
        {
            Console.Out.WriteLine("Starting CarEngine");
        }

        public void Forward()
        {
            Console.Out.WriteLine("Moving car forward");
        }
    }

    public interface IEngine
    {
        void Start();
        void Forward();
    }
}

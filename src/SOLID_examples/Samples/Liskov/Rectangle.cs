﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Samples.Liskov
{
    /// <summary>
    /// Definition: four sides and four right angles.
    /// Sides may be adjusted independently
    /// </summary>
    public class Rectangle
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    /// <summary>
    /// Definition: A rectangle (is-a) where the sides are of equal length
    /// Adjusting one side must forcibly adjust the other.
    /// </summary>
    public class Square:Rectangle
    {
        //..
    }
}

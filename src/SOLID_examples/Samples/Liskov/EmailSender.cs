﻿using System;
using System.Net.Mail;

namespace Samples.Liskov
{

    #region Interfaces and helper classes

    public interface IEmailGateway
    {
        MailResult Send(string to, string from, string subject, string body);
    }

    public class MailResult
    {
        public bool Success { get; set; }
    }

    #endregion

    public class EmailSender
    {
        #region Constructor and fields

        protected readonly IEmailGateway EmailGateway;
        // Number of sent emails
        public EmailSender(IEmailGateway emailGateway)
        {
            EmailGateway = emailGateway;
        }

        #endregion

        protected static int SentMailCount = 0;

        public void SendEmail(string to, string from, string subject, string body)
        {
            var result =EmailGateway.Send(to, from, subject, body);
            if (!result.Success)
            {
                throw new ApplicationException("Email not sent!");
            }
            SentMailCount++;
        }
    }

    public class HtmlMailSender : EmailSender
    {
        #region Constructor

        public HtmlMailSender(IEmailGateway emailGateway) : base(emailGateway)
        {
        }

        #endregion

        public new void SendEmail(string to, string from, string subject, string body)
        {

            // STRONGER PRECONDITION
            try
            {
                var mailaddress = new MailAddress(to);
            }
            catch (FormatException f)
            {
                throw new ArgumentException(string.Format("Invalid emailaddress 'to': '{0}'", to));
            }

            var htmlbody=body.Replace(Environment.NewLine,"<br>"+Environment.NewLine);
            
            // WEAKER POSTCONDITION, NO STATUS CHECK
            EmailGateway.Send(to, from, subject, htmlbody);
        
            // INVARIANS VIOLATION 
            SentMailCount = SentMailCount-1;
        }
    }
}









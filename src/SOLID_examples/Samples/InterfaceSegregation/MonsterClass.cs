﻿namespace Samples.InterfaceSegregation
{
    public class MonsterClass : IUsermanager, ITodolistPresenter, IListPrinter
    {
        public void PrintLists() { }
        public string GetCurrentUsername() { return "J.D."; }
        public void SetPassword(string pwd) { }
        public void SendResetPasswordEmail() { }
        public void RenderTodoList() { }
        public void OnCreateTaskClick() { }
    }

    #region Segregated interfaces


    public interface IUsermanager
    {
        void SetPassword(string pwd);
        void SendResetPasswordEmail();
        string GetCurrentUsername();
    }

    public interface ITodolistPresenter
    {
        void RenderTodoList();
        void OnCreateTaskClick();
    }

    public interface IListPrinter
    {
        void PrintLists();
    }

    public interface IFileManager
    {
        void DeleteTempFiles();
    }

    #endregion



    #region UserManagerConsole - example 'client'

    public class UserManagerConsole
    {
        private readonly IUsermanager _mgr;

        public UserManagerConsole(IUsermanager mgr)
        {
            _mgr = mgr;
        }

        public void SendResetpwd()
        {
            //var mc = new MonsterClass();
            //mc.SendResetPasswordEmail();
            _mgr.GetCurrentUsername();
        }
    }

    #endregion

    #region FileMgr - extraction from MonsterClass, replacing

    /// <summary>
    /// Implementering af IFileManager der skal erstatte MonsterClass i klient app
    /// </summary>
    public class FileMgr : IFileManager
    {
        public void DeleteTempFiles()
        {
            //  yay
        }
    }

    #endregion


}
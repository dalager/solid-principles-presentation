﻿using System;

namespace Samples
{
    public abstract class PayCalculator
    {
        protected readonly Person Person;

        protected PayCalculator(Person person)
        {
            Person = person;
        }

        public abstract decimal Calculate(DateTime date);
    }



    /// <summary>
    /// Regner ud på baggrund af arbejdede timer
    /// </summary>
    public class EmployeePayCalculator : PayCalculator
    {
        public EmployeePayCalculator(Person person) : base(person){}

        public override decimal Calculate(DateTime date)
        {
            // get days worked
            return new decimal((int) (100 * ((Employee)Person).Rate));
        }
    }

    /// <summary>
    /// Regner ud på baggrund af arbejdede timer/ejerforhold/overskud/magi
    /// </summary>
    public class PartnerPayCalculator : PayCalculator
    {
        public PartnerPayCalculator(Person person) : base(person){}
        
        public override decimal Calculate(DateTime date)
        {
            return (decimal)new Random().NextDouble();
        }
    }
}
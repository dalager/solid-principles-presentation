using System;
using System.Collections.Generic;

namespace Samples
{
    public class ProjektSystem
    {
        private readonly IPaymentSystem _paymentSystem;

        public ProjektSystem(IPaymentSystem paymentSystem)
        {
            _paymentSystem = paymentSystem;
        }

        public void PayPeopleNow()
        {

            foreach (var person in new List<Person>())
            {
                var employee = person as Employee;
                if (employee != null)
                {
                    _paymentSystem.Pay(employee.WorkedHours*employee.Rate,employee.Account);
                    continue;
                }
                 
                var partner = person as Partner;
                if (partner != null)
                {
                    var rate = new Random().Next(0, 100000);
                    _paymentSystem.Pay(rate,partner.Account);
                    continue;
                }
            }
        }


        #region Strategy

        //        var payCalculator = PayCalculatorFactory.Create(person);
        //var rate = payCalculator.Calculate(DateTime.Now);
        //_paymentSystem.Pay(rate, person.Account);

        #endregion

    }
}
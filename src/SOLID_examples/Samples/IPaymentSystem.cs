namespace Samples
{
    public interface IPaymentSystem
    {
        void Pay(decimal rate, string account);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Samples.Liskov;

namespace Samples.Tests
{
    [TestFixture]
    public class LiskovTests
    {
        [Test]
        public void GetAwayFast()
        {
            Vehicle escapeVehicle = GetEscapeVehicle();
            escapeVehicle.Start();
            escapeVehicle.GoTo(12.3333, 43.34444);
            
        }


        private static Vehicle GetEscapeVehicle()
        {
            return new Car();
        }


        [Test]
        public void HistoryRule()
        {
            var a = new A();
            a.SetBar("bar");

            var b = new B();
            b.SetFoo("foo");

        }
    }


    public class A
    {
        public string Foo { get; set; }
        public string Bar { get; set; }

        public void SetBar(string s)
        {
            Bar = s;

        }
    }

    public class B : A
    {
        public void SetFoo(string f)
        {
            Foo = f;
        }
    }
}

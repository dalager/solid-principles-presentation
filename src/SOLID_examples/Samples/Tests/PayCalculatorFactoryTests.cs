﻿using NUnit.Framework;

namespace Samples.tests
{
    [TestFixture]
    public class PayCalculatorFactoryTests
    {
        [Test]
        public void Strategy_Will_Respect_Partner()
        {
            var partner = new Partner();
            var payCalculator = PayCalculatorFactory.Create(partner);
            Assert.IsAssignableFrom<PartnerPayCalculator>(payCalculator);
            Assert.IsNotAssignableFrom(typeof(EmployeePayCalculator),payCalculator);
        }
    }
}
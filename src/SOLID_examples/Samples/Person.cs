﻿namespace Samples
{
    public abstract class Person
    {
        public int WorkedHours { get; set; }
        public string Account { get; set; }
    }

    public class Employee : Person
    {
        public decimal Rate { get; set; }
    }

    public class Partner : Person
    {
    }
}
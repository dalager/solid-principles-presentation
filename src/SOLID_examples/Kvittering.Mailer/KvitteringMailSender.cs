using KvitteringBusinessLogic;
using KvitteringDomain;

namespace KvitteringMailer
{
    public class KvitteringMailSender : IKvitteringMailSender
    {
        private readonly IMailTransporter _mailTransporter;

        public KvitteringMailSender(IMailTransporter mailTransporter)
        {
            _mailTransporter = mailTransporter;
        }

        public void Send(Kvittering kvittering)
        {
            _mailTransporter.Send("from@from.fr","to@to.to",kvittering.Butik,"Bel�b: "+kvittering.Bel�b);
        }
    }
}
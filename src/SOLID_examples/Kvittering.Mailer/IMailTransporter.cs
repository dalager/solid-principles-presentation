namespace KvitteringMailer
{
    public interface IMailTransporter
    {
        void Send(string from, string to, string subject, string body);
    }
}
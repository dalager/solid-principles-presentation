﻿using KvitteringBusinessLogic;
using KvitteringMailer;
using KvitteringMemoryStore;
using KvitteringSMTPMailer;

namespace KvitteringClient
{
    public class KvitteringClient{
        public void Process()
        {
            // Creational setup. "Composition Root"
            IMailTransporter mailtransporter = new SMTPMailTransporter();
            IKvitteringMailSender kvitteringMailSender = new KvitteringMailSender(mailtransporter);
            IKvitteringRepository kvitteringRepository = new InMemoryKvitteringRepository();


            var kvitteringService = new KvitteringService(kvitteringMailSender,kvitteringRepository);
            kvitteringService.SendUnsentKvitteringer();
        }
    }
}
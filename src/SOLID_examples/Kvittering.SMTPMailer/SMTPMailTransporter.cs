﻿using System.Net.Mail;
using KvitteringMailer;

namespace KvitteringSMTPMailer
{
    public class SMTPMailTransporter:IMailTransporter
    {
        public void Send(string @from, string to, string subject, string body)
        {
                        var smtpClient = new SmtpClient();
            smtpClient.Send(new MailMessage(@from, to, subject, body));

        }
    }
}

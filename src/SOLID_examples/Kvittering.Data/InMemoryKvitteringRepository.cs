﻿using System.Collections.Generic;
using KvitteringBusinessLogic;
using KvitteringDomain;

namespace KvitteringMemoryStore
{
    public class InMemoryKvitteringRepository : IKvitteringRepository
    {
        public IList<Kvittering> GetUnsentKvitteringer() { return new List<Kvittering>();} 
    }
}
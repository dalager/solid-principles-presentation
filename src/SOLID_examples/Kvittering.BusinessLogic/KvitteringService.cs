using System.Collections.Generic;
using KvitteringDomain;

namespace KvitteringBusinessLogic
{
    public class KvitteringService : IKvitteringService
    {
        private readonly IKvitteringMailSender _sender;
        private readonly IKvitteringRepository _kvitteringRepository;

        public KvitteringService(IKvitteringMailSender sender,IKvitteringRepository kvitteringRepository)
        {
            _sender = sender;
            _kvitteringRepository = kvitteringRepository;
        }

        public void SendUnsentKvitteringer()
        {
            IList<Kvittering> list = _kvitteringRepository.GetUnsentKvitteringer();
            foreach (var receipt in list)
            {
                SendKvittering(receipt);
            }
        }

        public void SendKvittering(Kvittering r)
        {
            _sender.Send(r);
        }
    }
}
using System.Collections.Generic;
using KvitteringDomain;

namespace KvitteringBusinessLogic
{
    public interface IKvitteringRepository
    {
        IList<Kvittering> GetUnsentKvitteringer();
    }
}
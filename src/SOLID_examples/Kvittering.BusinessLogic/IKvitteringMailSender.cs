    using KvitteringDomain;

namespace KvitteringBusinessLogic
{
    public interface IKvitteringMailSender
    {
        void Send(Kvittering kvittering);
    }
}
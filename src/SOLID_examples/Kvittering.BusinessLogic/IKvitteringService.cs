using KvitteringDomain;

namespace KvitteringBusinessLogic
{
    public interface IKvitteringService
    {
        void SendKvittering(Kvittering r);
        void SendUnsentKvitteringer();
    }
}